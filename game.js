//Class of Game of Life
class gameOfLife{

	constructor(data){
		this.COLS=COLS;
		this.ROWS=ROWS;
		this.ctx=ctx;
	}

	//method
	animation(){
		//build the grid with the measurements
		function generateGrid() {
		  return new Array(COLS).fill(null)
		    .map(() => new Array(ROWS).fill(null)
		      .map(() => Math.floor(Math.random() * 2)));
		}

		//the grid is stored in a variable
		let grid = generateGrid();

		//run the animation
		requestAnimationFrame(update);

		// game update
		function update() {
		  grid = nextUpdate(grid);
		  render(grid);
		  requestAnimationFrame(update);
		}

		// life init grid
		function nextUpdate(grid) {
		  const nextUpdate = grid.map(arr => [...arr]);

		 // touch each grid coord
		  for (let col = 0; col < grid.length; col++) {
		    for (let row = 0; row < grid[col].length; row++) {

		      //cell status	
		      const cell = grid[col][row];

		      let neighbours = 0;

		      //neighbors position
		      for (let i = -1; i < 2; i++) {
		        for (let j = -1; j < 2; j++) {
		          if (i === 0 && j === 0) {
		            continue;
		          }

		          //Neighbors position in grid
		          const position_x = col + i;
		          const position_y = row + j;

		          // if x and y on the grid
		          if (position_x >= 0 && position_y >= 0 && position_x < COLS && position_y < ROWS) {
		            const currentNeighbour = grid[col + i][row + j];
		            neighbours += currentNeighbour;
		          }
		        }
		      }

		      // rules
		      //Any live cell with fewer than two live neighbours dies, as if caused by underpopulation
		      if (cell === 1 && neighbours < 2) {
		        nextUpdate[col][row] = 0;
		      }
		       
		      //Any live cell with more than three live neighbours dies, as if by overcrowding
		      else if (cell === 1 && neighbours > 3) {
		        nextUpdate[col][row] = 0;
		      } 

		      //Any live cell with two or three live neighbours lives on to the next generation.
		      else if (cell === 1 && neighbours === 2 || neighbours === 3 ) {
				nextUpdate[col][row] = 1;
		 	  }
			  
			  //Any dead cell with exactly three live neighbours becomes a live cell
		 	  else if (cell === 0  && neighbours === 3) {
		        nextUpdate[col][row] = 1;
		      }

		    }
		  }
		  return nextUpdate;
		}

		//render of canvas configuration properties
		function render(grid) {
		  for (let col = 0; col < grid.length; col++) {
		    for (let row = 0; row < grid[col].length; row++) {
		      const cell = grid[col][row];

		      ctx.beginPath();
		      ctx.rect(col * resolution, row * resolution, resolution, resolution);
		      ctx.fillStyle = cell ? 'black' : 'white';
		      ctx.fill();
		    }
		  }
		}
	}
	
};

//get canvas
const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');

//resolutions and size
const resolution = 10;
canvas.width = 800;
canvas.height = 400;

//Columns and rows
const COLS = canvas.width / resolution;
const ROWS = canvas.height / resolution;

let data={COLS:COLS, ROWS:ROWS, ctx:ctx};

//create object -instantiate
let objet= new gameOfLife(data);

//call animation method
document.write(objet.animation());

